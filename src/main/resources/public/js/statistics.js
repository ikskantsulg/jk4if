$(function () {
    // creating a bunch of arrays to make data suitable for chart.js
    var cakeNames = [];
    var cakeAmounts = [];
    var cakePercentages = [];
    var randomColours = [];
    var totalAmount = 0;

    const loadOrderCakes = function () {
        return $.get('/orders/ordercakes');
    };

    const getCakeNames = function (orderCakes) {
        orderCakes.forEach(function (orderCake) {
            if(cakeNames.indexOf(orderCake.cake.name) === -1){
                cakeNames.push(orderCake.cake.name);
            }
        });
    };

    const getCakeAmounts = function (orderCakes) {
        cakeNames.forEach(function (cakeName) {
            cakeAmounts.push(
                orderCakes.reduce(function (total, orderCake){
                    return (orderCake.cake.name === cakeName) ? total + orderCake.amount : total;
                }, 0)
            );
        });
    };

    const getTotalAmount = function () {
        totalAmount = cakeAmounts.reduce(function (acc, amount){
            return acc + amount;
        });
    };

    const getCakePercentages = function (cakeAmounts, totalAmount) {
        cakePercentages = cakeAmounts.map(function (amount) {
            return (amount / totalAmount * 100).toFixed(2);
        });
    };

    const getRandomColours = function () {
        for(var i = 0; i < cakeNames.length; i++){
            var r = Math.floor(Math.random() * 255);
            var g = Math.floor(Math.random() * 255);
            var b = Math.floor(Math.random() * 255);
            randomColours.push('rgba(' + r + ',' + g + ',' + b + ', 0.5)');
        }
    };

    const refreshStats = function () {
        loadOrderCakes().then(function (orderCakes){
            getCakeNames(orderCakes);
            getCakeAmounts(orderCakes);
            getTotalAmount();
            getCakePercentages(cakeAmounts, totalAmount);
            getRandomColours();
        }).then(function (){
            showStats();
        });
    };

    const showStats = function () {
        $('#totAmount').text(totalAmount);

        // barChart settings
        var ctx1 = $('#barChart');
        var barChart = new Chart(ctx1, {
           type: 'bar',
           data: {
               labels: cakeNames,
               datasets: [{
                   data: cakeAmounts,
                   backgroundColor: randomColours,
                   borderWidth: 1
               }]
           },
            options: {
                legend: {
                    display: false
                },
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true,
                            fontSize: 18
                        }
                    }],
                    xAxes: [{
                        ticks: {
                            fontSize: 18
                        }
                    }]
                }
            }
        });

        // pieChart settings
        var ctx2 = $('#pieChart');
        var pieChart = new Chart(ctx2, {
            type: 'pie',
            data: {
                labels: cakeNames,
                datasets: [{
                    label: '% of Orders',
                    data: cakePercentages,
                    backgroundColor: randomColours,
                    borderWidth: 1
                }]
            },
            options: {
                legend: {
                    labels: {
                        fontSize: 18
                    }
                }
            }

        });
    };

    //when page is loaded
    refreshStats();
});